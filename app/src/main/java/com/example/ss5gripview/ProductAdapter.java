package com.example.ss5gripview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ProductAdapter extends BaseAdapter {

    private Context context;
    private int Layout;
    private List<Product> productList;

    public ProductAdapter(Context context, int layout, List<Product> productList) {
        this.context = context;
        Layout = layout;
        this.productList = productList;
    }

    @Override
    public int getCount() {
        return productList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    private class ViewHolder {
        ImageView ivImage;
        TextView tvTen, tvGia, tvMota;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        ViewHolder holder;
        if (view == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(Layout, null);
            holder = new ViewHolder();

            holder.tvTen = (TextView) view.findViewById(R.id.tvName);
            holder.tvGia = (TextView) view.findViewById(R.id.tvPrice);
            holder.tvMota = (TextView) view.findViewById(R.id.tvDes);
            holder.ivImage = (ImageView) view.findViewById(R.id.ivHinhAnh);
            view.setTag(holder);
        }else {
            holder = (ViewHolder) view.getTag();
        }

        Product product =productList.get(i);
        holder.tvTen.setText(product.getName());
        holder.tvGia.setText(product.getPrice());
        holder.tvMota.setText(product.getDes());
        holder.ivImage.setImageResource(product.getImage());

        return view;
    }
}
