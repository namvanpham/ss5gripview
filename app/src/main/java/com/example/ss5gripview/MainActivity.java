package com.example.ss5gripview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    GridView gvSanpham;
    ArrayList<Product> arrayProduct;
    ProductAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Anhxa();
        adapter = new ProductAdapter(this, R.layout.item_product, arrayProduct);
        gvSanpham.setAdapter(adapter);

        gvSanpham.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(MainActivity.this, arrayProduct.get(i).getName(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void Anhxa() {
        gvSanpham = (GridView) findViewById(R.id.gripviewProduct);
        arrayProduct = new ArrayList<>();

        arrayProduct.add(new Product("Black shirt", "500.000VNĐ", "Áo khá đẹp", R.drawable.shirt1));
        arrayProduct.add(new Product("White Shirt", "500.000VNĐ", "Áo khá trắng", R.drawable.shirt2));
        arrayProduct.add(new Product( "Body shirt", "700.000VNĐ", "Áo khá bó", R.drawable.shirt3));
        arrayProduct.add(new Product("Floral shirt", "150.000VNĐ", "Áo Khá Bảnh", R.drawable.shirt4));
    }
}
